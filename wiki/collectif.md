# Collectif

## Objets du collectif

Le collectif QueerMotion est en non-mixité [queer](/lexique.html#queer), composé de créateur·ices actifs pour administrer l'[instance QueerMotion.org](/instance.html) et pour proposer du contenu audiovisuel, en favorisant la collaboration et l'entraide.

## Site du collectif

Pour en savoir plus sur le collectif QueerMotion, c'est par ici : https://collectif.queermotion.org
