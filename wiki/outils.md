# Outils

## Import YouTube

**Infos vidéo**

- https://ytubetool.com titre, description, tags

**Sous-titres vidéo**

 - https://downsub.com service externe
 - https://studio.youtube.com studio youtube - dans onglet «subtitles» - pour récupérer les sources

## Partage des chaines

Pour partager une chaine - exemple pour **ledoecast**&nbsp;:

- Lien classique&nbsp;: https://queermotion.org/c/ledoecast
- Lien court&nbsp;: https://queermotion.org/@ledoecast


Pour partager un compte - exemple pour **kimsible**&nbsp;:

- Lien classique&nbsp;: https://queermotion.org/a/kimsible
- Lien court&nbsp;: https://queermotion.org/@kimsible
