# Soutien

Voici plusieurs moyens de nous soutenir simplement si le projet vous plait ✨ :

## Dons

**Avoir un service fiable et continu est une priorité** pour faire fonctionner cette instance PeerTube, stocker, encoder et diffuser un grand nombre de vidéos.

Ainsi, notre indépendance aux plateformes comme YouTube et indirectement **la survie de l'instance**, dépendent grandement de notre capacité à **pouvoir financer la location du serveur**.

Les dépenses (serveurs, sauvegardes, nom de domaine) sont d'environ **800€/an**.

[Voir les détails techniques](https://queermotion.org/about/instance#hardware-information).

**Pour nous aider** à couvrir les frais de location du serveur vous pouvez **participer financièrement** ponctuellement ou régulièrement sur&nbsp;:

[Liberapay](https://liberapay.com/QueerMotion) {.button .liberapay .shadow-bottom}

## Avant-premières

Dans le cas de publication multi-plateformes (YouTube, Soundcloud, Spotify, Dailymotion, Instagram...), je peux **publier en avant-première** sur QueerMotion.org :
  - À communiquer sur les réseaux sociaux (discord, twitter, facebook, mastodon, insta...)
  - **En exclusivité minimum 5 jours** avant de publier ailleurs

_**Exemple**&nbsp;: « [À DÉCOUVRIR] Ma nouvelle cover de David Bowie est disponible en avant-première sur https://QueerMotion.org ! [LIEN VIDÉO] »_

<h3>Vidéos exclusives</h3>

**Les vidéos exclusives sur QueerMotion.org** permettent de mettre en avant sa chaîne QueerMotion.org le plus efficacement.

## Mention @queermotion

![](/social-media-mention.png) {.float-left .screenshot}

En tant que créateur·ice, je peux mentionner @QueerMotion dans la description de mes comptes de réseaux sociaux&nbsp;:

• Sur Twitter et Instagram&nbsp;: **créateur·ice sur @queermotion**

• Sur Mastodon&nbsp;: **créateur·ice sur @queermotion@eldritch.cafe**

• Sur les autres réseaux sociaux&nbsp;: **créateur·ice sur QueerMotion.org**

<div style=clear:left></div>
