---
title: Bienvenue
---

<h2>Bienvenue sur le Wiki de QueerMotion.org !</h2>

Vous trouverez ici des compléments d'informations sur le fonctionnement de l'instance PeerTube QueerMotion.org : collectif, outils, lexique, soutien, modération...

::: tip En bref

- Les inscriptions sont ouvertes aux personnes **queers et leurs allié·es**
- Le contenu est [généraliste](instance.html#thematiques), la [**pornographie n'est pas autorisée**](instance.html#pornographie) et l'**érotisme est toléré**
- Les contenus multi-plateformes (YouTube, SoundCloud, Spotify, Viméo...) sont généralement publiés en **avant-première de 5 jours** sur QueerMotion.org
- Le recensement de contenus d'autres plateformes n'étant pas la vocation première de l'instance, **l'importation par URL est désactivée**
- Le [quota vidéo est limité à 500MB](instance.html#limitations-et-quotas) pour chaque nouvelle inscription  et chaque vidéo est **vérifiée** avant publication
    - **Peut évoluer au cas par cas, sur demande, après validation par le collectif**
- Les contenus sont mis à disposition sous [la licence Creative Commons](https://creativecommons.org/licenses/?lang=fr-FR) choisie par les créateur·ices

:::
