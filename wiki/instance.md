# Instance

::: tip QUEERMOTION.ORG
[QueerMotion.org](https://queermotion.org) est **une instance du réseau [PeerTube](/peertube.html)**.
:::

## Thématiques

Bien que réservée en priorité à des créateur·ices queers, QueerMotion.org est une instance abordant multiples thématiques:
- thèmes LGBTQIA+, incluant l'éducation sexuelle
- toutes sortes de disciplines artistiques, littérature, musique, cinéma, humour, cuisine, maquillage, informatique, jeux vidéos...

## Pornographie

La **pornographie n'est pas autorisée**, l'érotisme est toléré.

::: warning PORNOGRAPHIE ET ÉROTISME
L'âge minimum requis pour s'inscrire étant de 16 ans, les contenus mettant en scène des **rapports et / ou des pratiques sexuellement explicites et réservés à un public adulte ne sont pas autorisés**. Les contenus **interdit aux moins de 16 ans à la télévision [(Classification du CSA)](https://fr.wikipedia.org/wiki/Commission_de_classification_des_%C5%93uvres_cin%C3%A9matographiques#%C3%80_la_t%C3%A9l%C3%A9vision) et dans les jeux-vidéos [(PEGI)](https://pegi.info/fr/page/que-signifient-les-logos) sont tolérés**. Les contenus à caractère érotique rentrent dans cette catégorie mais selon les cas, peuvent avoir besoin d'être classés NSFW.
:::

## Code de conduite

1. L'envoi de gros fichiers - **plus de 1Go** - doit également se faire avec modération, soit **1 fichier à la fois**, et être **espacé dans le temps**.

2. L'utilisation du **direct/live** ne doit pas être trop fréquente, c'est-à-dire **espacée de quelques heures**.

3. Les échanges dans les commentaires doivent rester bienveillants.

4. Tout contenu à caractère discriminant et se rapportant à une [oppression systémique](/lexique.html#oppressions) - queerphobies, racisme, psychophobie, misogynie... - est proscrit.

5. Aucun contenu ne doit faire la promotion des sujets suivants :

  - maltraitance animale
  - mutilations, gore, trash, incluant le BDSM extrême
  - pédocriminalité, viol, harcèlement et agressions sexuelles
  - violences physiques, policières, militaires, guerrières, et verbales
  - armes blanches, et armes à feu.

## Modération

Les contenus sont modérés par les [membres du collectif](/collectif.html).

### Vidéos NSFW  (Not Safe For Work)

La case ***Cette vidéo contient du contenu sensible*** doit être cochée pour les vidéos et audios contenant de **manière répétée de la violence physique ou psychologique**, de la **nudité avec parties génitales explicites**, et **à caractère érotique** afin de flouter sa miniature et d'alerter l'utilisateur·ice avant la lecture.

### Trigger warnings

L'ajout de _trigger warnings_ dans la description d'une publication est aussi fortement conseillé si une partie du contenu ou vocabulaire peut choquer ou renforcer une [oppression systémique](/lexique.html#oppressions).

### Vérification des vidéos

Chaque vidéo est vérifiée par la modération avant publication, afin de s'assurer qu'elles respectent bien le [code de conduite](/instance.html#code-of-conduct) de l'instance.

**Peut évoluer au cas par cas, sur demande, après validation par le collectif**.


## Limitations et quotas

- Quota par vidéo ~ **4GB**
- Quota des vidéos ~ **500MB à illimité** (10GB / jour)
  - **Peut évoluer au cas par cas, sur demande, après validation par le collectif**
- Durée des directs / lives ~ **3h**
- Résolution des rediffusions générées par le serveur ~ **source uniquement**
    - _La source du live peut être aussi enregistrée avec [OBS Studio](https://obsproject.com)_

::: tip STOCKAGE VIDÉO
La capacité de stockage vidéo de l'instance est de **6000 Giga Bytes** soit **50 chaînes** de 120GB ~ **250 vidéos** d'une durée moyenne de **10 min** chacune.
:::
