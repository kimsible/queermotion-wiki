module.exports =  {
  title: 'Wiki QueerMotion.org',
  description: `Vous trouverez ici des compléments d'informations sur le fonctionnement de l'instance PeerTube QueerMotion.org : collectif, outils, lexique, soutien...`,
  lang: 'fr_FR',
  markdown: {
    config: (md) => {
      md.use(require('markdown-it-attrs'))
    }
  },
  themeConfig: {
    sidebar: [
      { text : 'Instance', link: '/instance.html' },
      { text : 'Collectif', link: '/collectif.html' },
      { text : 'PeerTube', link: '/peertube.html' },
      { text : 'Outils', link: '/outils.html' },
      { text : 'Soutien', link: '/soutien.html' },
      { text : 'Lexique', link: '/lexique.html' },
      { text : 'Mentions légales', link: '/mentions-legales.html' },
    ],
    head: [
      ['link', { rel: 'icon', href: '/logo.png' }]
    ],
    logo: '/logo.png',
    base: '/queermotion-wiki/'
  }
}
