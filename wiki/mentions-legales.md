# Mentions légales

## Logo

Le logo du collectif QueerMotion a été créé par [Solen DP](https://behance.net/solendp999) et utilise [les couleurs du chevron et du drapeau de la fierté queer](https://queerflag.tumblr.com/). Il est mis à disposition selon les termes de la Licence Creative Commons Attribution - Pas d'Utilisation Commerciale - Pas de Modification 4.0 International.

[![](/cc.png)](http://creativecommons.org/licenses/by-nc-nd/4.0/) {.cc}


## Wiki

[wiki.queermotion.org](https://wiki.queermotion.org) est le site du wiki QueerMotion.org hébergé sur [render.com](https://render.com).

Ce site n'utilise pas les cookies, et ne collecte pas de données personnelles.

## Collectif

[collectif.queermotion.org](https://collectif.queermotion.org) est le site du collectif QueerMotion hébergé sur [render.com](https://render.com).

Ce site n'utilise pas les cookies, et ne collecte pas de données personnelles.

## Instance PeerTube

[QueerMotion.org](https://queermotion.org) est le site de l'instance PeerTube du [collectif QueerMotion](https://collectif.queermotion.org).

<h4>Coordonnées de l'hébergeur</h4>

```
hetzner.com
Hetzner Online GmbH
Industriestr. 25
91710 Gunzenhausen, Deutschland
Tel.: +49 (0)9831 505-0
E-Mail: info@hetzner.com
```

<h4>Utilisation des données personnelles</h4>

Pour les visiteur·ses non-connecté·es, ce site n'utilise pas les cookies et ne collecte pas de données personnelles. Pour les membres inscrit·es et connecté·es, seule l'adresse e-mail utilisée pour la création de compte est stockée en base de donnée, ainsi que la dernière date de connexion au sein d'un cookie. Aucune donnée personnelle (adresse e-mail) ne sera conservée si la·e membre souhaite supprimer son compte.
